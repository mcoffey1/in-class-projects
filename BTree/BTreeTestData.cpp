/**********************************************
* File: BTreeTestData.cpp
* Author: Matthew Coffey
* Email: mcoffey1@nd.edu
* 
* This file contains the main driver function for
* the BTree Test for the data template 
**********************************************/
#include "BTree.h"
#include <stdlib.h>
#include <time.h>

struct Data{
	int key;
	int data;
/********************************************
 *  * * Function Name  : Data
 *   * * Pre-conditions : int, int
 *    * * Post-conditions: 
 *     * *
 *      * * Initialization Construtor for Data
 *       * ********************************************/

	Data(int key, int data) : key(key), data(data) {};
/********************************************
 *  * * Function Name  : Data
 *   * * Pre-conditions : none
 *    * * Post-conditions: 
 *     * *
 *      * *  Default Constructor for data
 *       * ********************************************/

	Data() : key(0), data(0) {};
/********************************************
 * * Function Name  : >, <, ==
 * * Pre-conditions : const Data&
 * * Post-conditions: bool
 * *
 * * These next three mehtods overload the comparison
 * * operators for the Data struct
 * ********************************************/
	bool operator>(const Data& d) const{
		return (key > d.key);
	}
	bool operator<(const Data& d) const{
		return (key < d.key);
	}
	bool operator==(const Data& d) const{
		return (key == d.key);
	}
/********************************************
 * * Function Name  : <<
 * * Pre-conditions : std::ostream&, const Data&
 * * Post-conditions: std::ostream&
 * *
 * * overloading cout for Data struct
 * ********************************************/
	friend std::ostream& operator<< (std::ostream& o, const Data& d){
		o << "Key: " << d.key << ", Data: " << d.data;
		return o;
  	}
};
/********************************************
* Function Name  : insert/deleteAndPrint
* Pre-conditions : BTree<Data> *t, Data& value
* Post-conditions: none
* 
* Takes a pointer to a BTree and a value,
* insertes/removes the value into the BTree, and then
* then prints the results 
********************************************/
void insertAndPrint(BTree<Data> *t, Data& value){
	
	t->insert(value); 
    	std::cout << "Traversal of tree after inserting " << value << ": \n"; 
    	t->traverse(); 
    	std::cout << std::endl; 
}

void deleteAndPrint(BTree<Data> *t, Data& value){
	
	t->remove(value); 
    std::cout << "Traversal of tree after deleting " << value << ": \n"; 
    t->traverse(); 
    std::cout << std::endl; 
}

// Driver program to test above functions 
/********************************************
 * * Function Name  : main
 * * Pre-conditions : int, char**
 * * Post-conditions: main
 * *
 * * Driver to test BTree and Data Struct
 * ********************************************/

int main(int argc, char** argv) 
{ 
	int minDeg =2;
    // A B-Tree with minium degree 3
	BTree<Data> tree(minDeg);
	// 50, 60, 70, 40, 30, 20, 10, 35, 80. Then, delete 60.
	int data, key, count =0;
	srand(time(NULL));
	while (count < 1000){
		data = rand() % 10000 + 1;
		key = rand() % 10000 + 1;
		Data d {key, data};
		tree.insert(d);
		//insertAndPrint(&tree, d);
		count++;
	}
	//Test Case 1
//	while (count < 10){
//                data = rand() % 10000 + 1;
//                key = rand() % 10000 + 1;
//                Data d {key, data};
//                insertAndPrint(&tree, d);
//                count++;
//        }

// Test Case 2
//	Data d {10001, 10001};
//	tree.insert(d);                      
//	count = 0;
//	if(tree.search(d) != NULL){
//                        std::cout <<"\n" << key <<" is present ";
//                        for (int i =0; i < 2*minDeg-1; i++){
//                                if ((ptr->keys[i]) == d){
//                                        std::cout << (ptr->keys[i]);
//                                }
//                        }
//                }
//
//        else
//                std::cout<<"\n" << key << " is not present";

// Test Case 3
// 	Data d { 500, 0};
// 	tree.remove(d);
//		if(tree.search(d) != NULL){
//                        std::cout <<"\n" << key <<" is present ";
//                        for (int i =0; i < 2*minDeg-1; i++){
//                                if ((ptr->keys[i]) == d){
//                                        std::cout << (ptr->keys[i]);
//                                }
//                        }
//                }
//
//               else
//                         std::cout<<"\n" << key << " is not present";
	count = 0;
	while (count < 10){
		key = rand() % 10000 + 1;
		Data d = {key, 0};
		BTreeNode<Data>* ptr;
		ptr = tree.search(d);
		if(tree.search(d) != NULL){
			std::cout <<"\n" << key <<" is present ";
			for (int i =0; i < 2*minDeg-1; i++){
				if ((ptr->keys[i]) == d){
					std::cout << (ptr->keys[i]);
				}
			}
		}
		
		else
			 std::cout<<"\n" << key << " is not present";
		count++;
	}
	std::cout<<std::endl;
    return 0; 
} 
