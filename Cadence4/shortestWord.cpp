/**********************************************
* File: shortestWord.cpp
* Author: Matthew Coffey
* Email: mcoffey1@nd.edu
*  
**********************************************/

#include<unordered_map>
#include<vector>
#include<string>
#include<iostream>
#include<fstream>
using namespace std;

/********************************************
* Function Name  : shortestWord
* Pre-conditions : string license, vector<string>& words
* Post-conditions: string
*  
* This gives us the shortest word from words that
* has all of the letters that were in the string 
* license
********************************************/

string shortestWord(string license, vector<string>& words){
	unordered_map<char, int> dict;
	for (char c : license){
		if(isupper(c))
			dict[c]++;
	}
	string result ="";
	for (string &s : words){
		unordered_map<char, int> temp = dict;
		for (char c : s){
			if(temp.count(c)){
				temp[c]--;
			}
		}
		int size = 0;
		for (auto &p : dict){
			char k = p.first;
			if(temp[k] > 0)
				break;
			size++;
		}
		if (size == dict.size()){
			if(result.empty() || s.size() < result.size())
				result = s;
		}
	}
	return result;
}
/********************************************
* Function Name  : main
* Pre-conditions : int argc, char *argv[]
* Post-conditions: int
*  
* This is the main driver program for the 
* program to find the shortest word with the letters
* of the license plate provided
********************************************/

int main (int argc, char* argv[]){
	ifstream ifs ("words.txt", ifstream::in);
	string s ="";
	vector<string> words;
	while( ifs >> s)
		words.push_back(s);
	ifs.close();
	string license = "";
	cout << "ENTER A LICENSE PLATE" << endl;
	cin >> license;
	cout<< shortestWord(license, words)<<endl;
}
