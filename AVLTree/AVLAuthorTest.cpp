/**********************************************
* File: AVLAuthorTest.cpp
* Author: 
* Email: 
*  
**********************************************/
#include <iostream>
#include <string>
#include "AVLTree.h"

// Struct goes here
struct author{
	std::string firstName;
	std::string lastName;

	author(std::string firstName, std::string lastName) : firstName(firstName), lastName(lastName) {};
	bool operator<(const author& a) const{
		if (lastName < a.lastName)
			return true;
		else if (lastName == a.lastName){
			if (firstName < a.firstName)
				return true;
		}
		return false;	
	}
	bool operator==(const author& a) const{
		if (lastName != a.lastName)
			return false;
		else{
			if (firstName != a.firstName)
				return false;
		}
		return true;
	}
	friend std::ostream& operator<< (std::ostream& o, const author& a){
		o << a.lastName << ", " << a.firstName;
		return o;
	}
};	
/********************************************
* Function Name  : main
* Pre-conditions : int argc, char **argv
* Post-conditions: int
*  
* This is the main driver program for the 
* AVLTree function with strings 
********************************************/
int main(int argc, char **argv)
{  
    AVLTree<author> testAVL;
    author a1 { "Matthew", "Coffey"};
    author a2 { "John", "Coffey"};
    author a3 { "Matthew", "Morrison"};
    author a4 { "JK", "Rowling" };
    author a5 { "George", "Martin" };
    testAVL.insert(a1);
    testAVL.insert(a2);
    testAVL.insert(a3);
    testAVL.insert(a4);
    testAVL.insert(a5);
    testAVL.printTree();
    std::cout<<"----"<<std::endl;
    testAVL.remove(a3);
    testAVL.printTree();
    return 0;
}
