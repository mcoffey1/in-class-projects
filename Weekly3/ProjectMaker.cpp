/********************************************
* Author: Matthew Coffey
* Email: mcoffey1
*
* Contains an STL implementation of an Adjacency List
* directed graph using C++ STL Vector
*
* Compile:
* g++ -g -std=c++11 ProjectMaker.cpp -o ProjectMaker
*
* Execute:
* ./ProjectMaker [list file]
*
* Code modified from Professor Matthew Morrison's AdjListDFS.cpp:
* https://gitlab.com/mmorri22/inclasswork/blob/master/GraphSTL/AdjListDFS.cpp
**********************************************/

#include <iostream>
#include <vector>
#include <stack>
#include <set>
#include <unordered_map>
#include <fstream>
#include <string>
#include <list>
// data structure to store graph edges
template<class T>
struct Edge {
	T src;
	T dest;
	int weight;

	template <typename U>
	/********************************************
	* Function Name  : operator<<
	* Pre-conditions :  std::ostream&, const Edge<U>&
	* Post-conditions: friend std::ostream&
	* Overloaded Friend Operator<< to print an edge
	********************************************/
	friend std::ostream& operator<<( std::ostream&, const Edge<U>& );
};

template <typename T>
std::ostream& operator<<( std::ostream& os, const Edge<T>& theEdge) {
	std::cout << "{" << theEdge.src << " " << theEdge.dest << " " << theEdge.weight << "} ";
	return os;
}


template<class T>
struct Vertex {
	T value;
	mutable std::vector< Edge <T> > edges;

	/********************************************
	* Function Name  : operator==
	* Pre-conditions : const Vertex<T>& rhs
	* Post-conditions: bool
	*
	* Overloaded == operator to compare vertex value
	********************************************/
	bool operator==(const Vertex<T>& rhs) const{
		return value == rhs.value;
	}

	/********************************************
	* Function Name  : operator<
	* Pre-conditions : const Vertex<T>& rhs
	* Post-conditions: bool
	*
	* Overloaded < operator to compare vertex values
	********************************************/
	bool operator<(const Vertex<T>& rhs) const{
		return value < rhs.value;
	}
};

// class to represent a graph object
template<class T>
class Graph
{
	public:
		// construct a vector to represent an adjacency list
		std::vector< Vertex<T> > adjList;

		// hash table to correlate vertices with location in vector
		std::unordered_map< T, int > hashVerts;

		/********************************************
		* Function Name  : Graph
		* Pre-conditions : const std::vector< Edge<T> > &edges
		* Post-conditions: none
		*
		* Graph Constructor
		********************************************/
		Graph(std::vector< Edge<T> > &edges)
		{
			// add edges to the directed graph
			for (auto &edge: edges)
			{
				// Insert Origin
				addEdge(edge);
			}
		}

		/********************************************
		* Function Name  : addEdge
		* Pre-conditions : Edge<T> edge
		* Post-conditions: none
		*
		********************************************/
		void addEdge(const Edge<T>& edge){
			// Create a temporary vertex with the source
			Vertex<T> tempVert;
			tempVert.value = edge.src;

			// Element was not found
			if(hashVerts.count(tempVert.value) == 0){

				// HashWords.insert( {wordIn, 1} );
				hashVerts.insert( {tempVert.value, adjList.size()} );

				// Inset the edge into the temporary vertex
				// Serves as the first edge
				tempVert.edges.push_back(edge);

				// Insert the vertex into the set
				adjList.push_back(tempVert);
			}

			// Element was found
			else{
				// Use the hash to get the location in adjList, then push onto the edges vector
				adjList.at(hashVerts[tempVert.value]).edges.push_back(edge);
			}
		}

		int returnHashLocation(T value){
		    return hashVerts[value];
		}
/********************************************
 * * Function Name  : inGraph
 * * Pre-conditions : T value
 * * Post-conditions: bool
 * *
 * * Poorly named; really checks if item not in adjList
 * ********************************************/

		bool inGraph(T value){
			for(int iter = 0; iter < adjList.size(); iter++){

                		for(int jter = 0; jter < adjList.at(iter).edges.size(); jter++){
//                      std::cout<<graph.adjList.at(iter).edges.at(jter).dest<< " == " << graph.adjList.at(iter).value<<std::endl;
                			if (adjList.at(iter).edges.at(jter).src == value)
						return false;
			}
		}
			return true;
	}
};

/********************************************
* Function Name  : printGraph
* Pre-conditions : const Graph<T>& graph
* Post-conditions: none
*
* Prints all the elements in the graph
********************************************/
template<typename T>
void printGraph(const Graph<T>& graph){
	std::cout << "Number of buckets is: " << graph.adjList.size() << std::endl;
	std::cout << "Origin: {Origin, Destination, Weight}" << std::endl;

	for(int iter = 0; iter < graph.adjList.size(); iter++){
		std::cout << graph.adjList.at(iter).value << ": ";

		for(int jter = 0; jter < graph.adjList.at(iter).edges.size(); jter++){
			std::cout << graph.adjList.at(iter).edges.at(jter);
		}

		std::cout << std::endl;
	}

	std::cout << std::endl;
}

/********************************************
* Function Name  : GraphDFS
* Pre-conditions : Graph<T>& graph, std::vector<T>& DFSOrder
* Post-conditions: none
*
* Recursive basic function for DFS Traversal
* Stores traversal results in DFSOrder
********************************************/
template<class T>
void GraphDFS(Graph<T>& graph, std::vector< Edge<T> >& DFSOrder, int top){
	std::unordered_map<T,bool> visitedVerts;
	visitedVerts.insert({ graph.adjList.at(top).value, false });
	GraphDFS(graph, top, DFSOrder, visitedVerts);
}
	
	
/********************************************
* Function Name  : GraphDFS
* Pre-conditions : Graph<T>& graph, int vecLoc, std::vector<T>& DFSOrder,
*				   std::unordered_map<T, bool>& visitedVerts
* Post-conditions: none
*  This is not quite a real DFS traversal anymore
*  I modified it to not add an element to the
*  vector unless its dependencies have already been 
*  traversed
********************************************/
template<class T>
void GraphDFS(Graph<T>& graph, int vecLoc, std::vector< Edge<T> >& DFSOrder, std::unordered_map<T, bool>& visitedVerts) {
	Vertex<T>* tempVert = &graph.adjList.at(vecLoc);
//	std::cout<<tempVert->value<<std::endl;
	bool hasDep = true;
	//Added this block to check for dependencies
	for(int iter = 0; iter < tempVert->edges.size(); iter++) {
//		std::cout<<tempVert->edges.at(iter).dest<<" " <<visitedVerts.count(tempVert->edges.at(iter).dest)<<std::endl; 
		if (visitedVerts.count(tempVert->edges.at(iter).dest) != 1)
			break;
		hasDep = false;
	}
//	std::cout<<"----------"<<std::endl;
	for(int iter = 0; iter < tempVert->edges.size(); iter++) {
		T tempDest = tempVert->edges.at(iter).dest;
	
		if (visitedVerts.count( tempDest ) == 0) {
			if(graph.inGraph(tempDest) || hasDep){ /* added this line */
				visitedVerts.insert( {tempDest, true} );
				DFSOrder.push_back( tempVert->edges.at(iter));
			} /* and this one*/
			GraphDFS(graph, graph.returnHashLocation(tempDest), DFSOrder, visitedVerts);
			
		}
	}
}
template<class T>
/********************************************
 * * Function Name  : FindTop
 * * Pre-conditions : Graph<T>& graph, int VecLoc
 * * Post-conditions: int
 * *
 * * Recursive function that finds the top level
 * * Project in the list
 * ********************************************/

int FindTop(Graph<T>& graph, int VecLoc){
//	std::cout<<VecLoc<<std::endl;
	 for(int iter = 0; iter < graph.adjList.size(); iter++){
                for(int jter = 0; jter < graph.adjList.at(iter).edges.size(); jter++){
//			std::cout<<graph.adjList.at(iter).edges.at(jter).dest<< " == "<<graph.adjList.at(VecLoc).value<<std::endl;
                        if (graph.adjList.at(iter).edges.at(jter).dest == graph.adjList.at(VecLoc).value)
				return FindTop(graph, ++VecLoc);
                }
        }
//	 std::cout<<VecLoc<<std::endl;
	return VecLoc;
}

template<class T>
/********************************************
* Function Name  : printDFS
* Pre-conditions : std::vector<T> DFSOrder
* Post-conditions: none
*
* Prints the results of our semi-DFS traversal in 
* a way that shows the compilation order
********************************************/
void printDFS(const std::vector<T>& DFSOrder){
	std::cout << "The Order of Compilation is: \n";
	//Modification: print only dests and Print top src last
	for(int iter = 0; iter < DFSOrder.size(); iter++){
		std::cout << DFSOrder.at(iter).dest << "\n";
	}
	std::cout << DFSOrder.at(0).src;
	std::cout << std::endl << std::endl;
}

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argv
* Post-conditions: int
*
* Main Driver Function
********************************************/
int main(int argc, char** argv)
{
	if (argc != 2){
		std::cout<<"Usage: "<<argv[0]<< " [list file]"<<std::endl;
		exit(1);
	}
	std::ifstream ifs;
	ifs.open(argv[1]);
	if (!ifs){
		std::cout << "Unable to open file";
		exit(1);
	}
	std::string proj;
	std::string dep;
	std::vector< Edge<std::string> > edges;
	Edge<std::string> e;
	while (ifs >> dep >> proj){
		e = {proj, dep, 1};
		edges.push_back(e);
	}
	ifs.close();

	// construct graph
	Graph<std::string> stringGraph(edges);

	// print adjacency list representation of graph
	printGraph(stringGraph);

	// Get DFS Order of the edges
	int top = FindTop(stringGraph, 0);
//	std::cout<<top<<std::endl;
	std::vector< Edge<std::string> > stringDFSOrder;
	GraphDFS(stringGraph, stringDFSOrder, top);
	printDFS(stringDFSOrder);

	return 0;
}

