/**********************************************
* File: QuadraticProbing.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
*  
**********************************************/
#include "QuadraticProbing.h"
#include <iostream>
using namespace std;

<<<<<<< HEAD
/**
 * Internal method to test if a positive number is prime.
 * Not an efficient algorithm.
 */
=======
>>>>>>> 818388bcdb607af1db62fcd46f4f15f1eaa10a81
/********************************************
* Function Name  : isPrime
* Pre-conditions :  int n 
* Post-conditions: bool
<<<<<<< HEAD
*  
=======
* 
* Internal method to test if a positive number is prime.
* Not an efficient algorithm. 
>>>>>>> 818388bcdb607af1db62fcd46f4f15f1eaa10a81
********************************************/
bool isPrime( int n )
{
    if( n == 2 || n == 3 )
        return true;

    if( n == 1 || n % 2 == 0 )
        return false;

    for( int i = 3; i * i <= n; i += 2 )
        if( n % i == 0 )
            return false;

    return true;
}

<<<<<<< HEAD
/**
 * Internal method to return a prime number at least as large as n.
 * Assumes n > 0.
 */
=======
>>>>>>> 818388bcdb607af1db62fcd46f4f15f1eaa10a81
/********************************************
* Function Name  : nextPrime
* Pre-conditions :  int n 
* Post-conditions: int
<<<<<<< HEAD
*  
=======
*
* Internal method to return a prime number at least as large as n.
* Assumes n > 0. 
>>>>>>> 818388bcdb607af1db62fcd46f4f15f1eaa10a81
********************************************/
int nextPrime( int n )
{
    if( n % 2 == 0 )
        ++n;

    for( ; !isPrime( n ); n += 2 )
        ;

    return n;
}
