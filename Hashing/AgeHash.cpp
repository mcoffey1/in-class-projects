<<<<<<< HEAD


/**********************************************
 * * File:AgeHash.cpp
 * * Author: MattheW Coffey
 * * Email: mcoffey1@nd.edu
 * *  
 * **********************************************/
#include <map>
#include <unordered_map>
#include <iterator>
#include <string>
#include <iostream>

int main(int argc, char** argv){
	std::map<std::string, int> ageHashOrdered = { {"Matthew", 38}, {"Alfred", 72}, {"Roscoe", 36}, {"James", 38}};
	std::map<std::string, int>::iterator iterOr;
	std::cout<<"The ordered ages are: " <<std::endl;
	for (iterOr = ageHashOrdered.begin(); iterOr != ageHashOrdered.end(); iterOr++){
		std::cout<< iterOr->first << " " <<iterOr->second << std::endl;
	}
	std::unordered_map<std::string, int> ageHashUnordered = { {"Matthew", 38}, {"Alfred", 72}, {"Roscoe", 36}, {"James", 38}};
	std::unordered_map<std::string, int>::iterator iterUn;
	for (iterUn = ageHashUnordered.begin(); iterUn != ageHashUnordered.end(); iterUn++){
		std::cout << iterUn->first << " " << iterUn->second << std::endl;
	}
	std::cout<<"The Unordered example: " << ageHashUnordered["Alfred"] << std::endl;
	std::cout<<"The ordered example: " << ageHashOrdered["Matthew"] << std::endl;

	return 0;
}
=======
/**********************************************
* File: AgeHash.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* Shows the comparison of the input or ordered and
* unordered sets 
**********************************************/
// Libraries Here

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argv
* Post-conditions: int
*
* Main driver function. Solution  
********************************************/
int main(int argc, char** argv){

	
	return 0;
}
>>>>>>> 818388bcdb607af1db62fcd46f4f15f1eaa10a81
