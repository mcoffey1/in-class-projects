/************************************
 * File Name: Lecture0-B.h  
 * Author: Matthew Morrison
 * E-mail: matt.morrison@nd.edu
 *
 * This file contains the functions and const variables
 * to be used in the Lecture0-B.cc example
 * 
 * Modified for C++, including libraries, and std namespace
 * All previous C code noted with   // PREVIOUS C code
 * New C++ code noted with          // EQUIVALENT C++ code
 *
 * This code is a modified version of the EX0206do.c function
 * provided by Cadence Design Systems, written by mikep
 *************************************/

#ifndef LECTURE0-B_H
#define LECTURE0-B_H

// PREVIOUS C code
//#include<stdio.h> 
// EQUIVALENT C++ code
#include <iostream> // cin, cout

// PREVIOUS C code
//#include<stdlib.h>
// EQUIVALENT C++ code
#include <cstdlib> // Included for malloc 

// Define ASCII characters
const int INT_Y = 121;
const int INT_N = 110;
const int INT_NULL = 0;

/*************************
 *  Function Name: allocateStrMemory
 *  Preconditions: char **, size_t length 
 *  Postconditions: None
 * 
 *  Allocates memory for a string of a given length 
 * ************************/
void allocateStrMemory(char ** str, const size_t length){
    *str = (char *)malloc(length * sizeof(char));
}

/*************************
 *  Function Name: deallocateStrMemory
 *  Preconditions: char **
 *  Postconditions: None
 * 
 *  Deallocates memory for a string
 * ************************/
void deallocateStrMemory(char ** str){
    free(*str);
}

/*************************
 *  Function Name: printStrings
 *  Preconditions: char *, char *
 *  Postconditions: None
 * 
 *  Prints the address and value of the two input strings
 * ************************/
void printStrings(char *str, char *str2){
    // PREVIOUS C code
    // fprintf(stdout, "str : %p, '%s'\n", str, str);
    // EQUIVALENT C++ code
    std::cout << "str : " << &str << ", '" << str << "'" << std::endl;
    
    // PREVIOUS C code
    // fprintf(stdout, "str2: %p, '%s'\n", str2, str2);
    // EQUIVALENT C++ code
    std::cout << "str2: " << &str2 << ", '" << str2 << "'" << std::endl;
}

/*************************
 *  Function Name: getStringScan
 *  Preconditions: char *
 *  Postconditions: None
 * 
 *  gets the input string with one character,
 *  and then discards the remaining characters
 * ************************/
void getStringScan(char *str){
    char temp;
    // PREVIOUS C code
    // fscanf(stdin, "%1s", str);
    // EQUIVALENT C++ code
    std::cin.get(str, 2); // Get the first character and allocate for a null.
    
    // Iterate through remainder of input if the input is longer than one char and enter
    while((temp = getchar()) != '\n')
        ;
}

#endif