/****************************************
 * File name: BST.h  
 * Author: Matthew Morrison 
 * Contact E-mail: matt.morrison@nd.edu 
 * 
 * This file contains the class members and 
 * functions for an implementation of a 
 * Binary Search Tree.
 * *************************************/
 
#ifndef BST_H
#define BST_H

#include <algorithm>
#include "BSTNode.h"
#include "DSExceptions.h"

template <typename T>
class BST
{
  public:

    /**********************************************
     * Function Name:  BST()
     * Pre-conditions: none
     * Post-conditions: none
     * 
     * Creates an empty BST class with a null root
     * ********************************************/  
    BST( ) : root{ nullptr }
    {
    }

    /**********************************************
     * Function Name:  BST
     * Pre-conditions: const BST & 
     * Post-conditions: none
     * 
     * This is the copy constructor for BST 
     * ********************************************/  
    BST( const BST & rhs ) : root{ nullptr }
    {
        root = clone( rhs.root );
    }

    /**********************************************
     * Function Name:  BST
     * Pre-conditions: BST &&
     * Post-conditions: none
     * 
     * This is the move constructor for BST
     * ********************************************/  
    BST( BST && rhs ) : root{ rhs.root }
    {
        rhs.root = nullptr;
    }
    
   /**********************************************
     * Function Name:  ~BST
     * Pre-conditions: none
     * Post-conditions: none
     * 
     * This is the destructor for BST
     * ********************************************/   
    ~BST( )
    {
        makeEmpty( );
    }

   /**********************************************
     * Function Name:  operator=
     * Pre-conditions: const BST &
     * Post-conditions: BST &
     * 
     * Copy assignment
     *******************************************/
    BST & operator=( const BST & rhs )
    {
        BST copy = rhs;
        std::swap( *this, copy );
        return *this;
    }
        
   /**********************************************
     * Function Name:  operator=
     * Pre-conditions: BST &&
     * Post-conditions: BST &
     * 
     * Move assignment operator
     *******************************************/
    BST & operator=( BST && rhs )
    {
        std::swap( root, rhs.root );       
        return *this;
    }
    
    
   /**********************************************
     * Function Name:  findMin
     * Pre-conditions: none
     * Post-conditions: const T&
     * 
     * Find the smallest item in the tree.
     * Throw UnderflowException if empty.
     ********************************************/
    const T & findMin( ) const
    {
        if( isEmpty( ) )
            throw UnderflowException{ };
        return findMin( root )->element;
    }

   /**********************************************
     * Function Name:  findMax
     * Pre-conditions: none
     * Post-conditions: const T&
     * 
     * Find the larges item in the tree.
     * Throw UnderflowException if empty.
     ********************************************/
    const T & findMax( ) const
    {
        if( isEmpty( ) )
            throw UnderflowException{ };
        return findMax( root )->element;
    }

   /**********************************************
     * Function Name:  contains
     * Pre-conditions: const T&
     * Post-conditions: bool
     * 
     * Returns true if x is found in the tree.
     ******************************************/
    bool contains( const T & x ) const
    {
        return contains( x, root );
    }

   /**********************************************
     * Function Name:  isEmpty
     * Pre-conditions: none
     * Post-conditions: bool
     * 
     * Test if the tree is logically empty.
     * Return true if empty, false otherwise.
     ********************************************/
    bool isEmpty( ) const
    {
        return root == nullptr;
    }
    
    /**********************************************
     * Function Name:  operator>>
     * Pre-conditions: std::istream& , Bottle& 
     * Post-conditions: std::ostream& 
     * 
     * This is the overloaded << operator function
     * ********************************************/        
    friend std::ostream& operator<< (std::ostream& stream, const BST& theBST){
        
        if( theBST.isEmpty( ) )
            stream << "Empty tree" << std::endl;
        else
            theBST.printTree( theBST.root , stream );
        
        return stream;
    }

   /**********************************************
     * Function Name:  makeEmpty
     * Pre-conditions: none
     * Post-conditions: bool
     * 
     * Make the tree logically empty.
     *********************************************/
    void makeEmpty( )
    {
        makeEmpty( root );
    }

   /**********************************************
     * Function Name:  insert
     * Pre-conditions: const T&
     * Post-conditions: void
     * 
     * Insert x into the tree; duplicates are ignored.
     *************************************************/
    void insert( const T & x )
    {
        insert( x, root );
    }
     
   /**********************************************
     * Function Name:  makeEmpty
     * Pre-conditions: T && 
     * Post-conditions: bool
     * 
     * Move insertion
     * Insert x into the tree; duplicates are ignored.
     *************************************************/
    void insert( T && x )
    {
        insert( std::move( x ), root );
    }
    
   /**********************************************
     * Function Name:  remove
     * Pre-conditions: const T &
     * Post-conditions: none
     * 
     *  Remove x from the tree. Nothing is done if x is not found.
     ****************************************************/
    void remove( const T & x )
    {
        remove( x, root );
    }


  private:

    BSTNode<T> *root;


   /**********************************************
     * Function Name:  insert
     * Pre-conditions: const T & x, BSTNode<T> * & t
     * Post-conditions: none
     * 
     * Internal method to insert into a subtree.
     * x is the item to insert.
     * t is the node that roots the subtree.
     * Set the new root of the subtree.
     ***********************************************/
    void insert( const T & x, BSTNode<T> * & t )
    {
        if( t == nullptr )
            t = new BSTNode<T>{ x, nullptr, nullptr };
        else if( x < t->element )
            insert( x, t->left );
        else if( t->element < x )
            insert( x, t->right );
        else
            ;  // Duplicate; do nothing
    }
    
   /**********************************************
     * Function Name:  insert
     * Pre-conditions: T && x, BSTNode<T> * & t
     * Post-conditions: none
     * 
     * Move constructor
     * Internal method to insert into a subtree.
     * x is the item to insert.
     * t is the node that roots the subtree.
     * Set the new root of the subtree.
     ***********************************************/
    void insert( T && x, BSTNode<T> * & t )
    {
        if( t == nullptr )
            t = new BSTNode<T>{ std::move( x ), nullptr, nullptr };
        else if( x < t->element )
            insert( std::move( x ), t->left );
        else if( t->element < x )
            insert( std::move( x ), t->right );
        else
            ;  // Duplicate; do nothing
    }

   /**********************************************
     * Function Name:  remove
     * Pre-conditions: const T & x, BSTNode<T> * & t
     * Post-conditions: none
     * 
     * Internal method to remove from a subtree.
     * x is the item to remove.
     * t is the node that roots the subtree.
     * Set the new root of the subtree.
     **********************************************/
    void remove( const T & x, BSTNode<T> * & t )
    {
        if( t == nullptr )
            return;   // Item not found; do nothing
        if( x < t->element )
            remove( x, t->left );
        else if( t->element < x )
            remove( x, t->right );
        else if( t->left != nullptr && t->right != nullptr ) // Two children
        {
            t->element = findMin( t->right )->element;
            remove( t->element, t->right );
        }
        else
        {
            BSTNode<T> *oldNode = t;
            t = ( t->left != nullptr ) ? t->left : t->right;
            delete oldNode;
        }
    }

   /*************************************************************
     * Function Name:  findMin
     * Pre-conditions: BSTNode<T> *
     * Post-conditions: BSTNode<T> *
     * 
     * Internal method to find the smallest item in a subtree t.
     * Return node containing the smallest item.
     ************************************************************/
    BSTNode<T> * findMin( BSTNode<T> *t ) const
    {
        if( t == nullptr )
            return nullptr;
        if( t->left == nullptr )
            return t;
        return findMin( t->left );
    }

   /*************************************************************
     * Function Name:  findMax
     * Pre-conditions: BSTNode<T> *
     * Post-conditions: BSTNode<T> *
     * 
     * Internal method to find the largest item in a subtree t.
     * Return node containing the largest item.
     */
    BSTNode<T> * findMax( BSTNode<T> *t ) const
    {
        if( t != nullptr )
            while( t->right != nullptr )
                t = t->right;
        return t;
    }


   /*************************************************************
     * Function Name:  contains
     * Pre-conditions: const T &, BSTNode<T> *
     * Post-conditions: BSTNode<T> *
     * 
     * Internal method to test if an item is in a subtree.
     * x is item to search for.
     * t is the node that roots the subtree.
     */
    bool contains( const T & x, BSTNode<T> *t ) const
    {
        if( t == nullptr )
            return false;
        else if( x < t->element )
            return contains( x, t->left );
        else if( t->element < x )
            return contains( x, t->right );
        else
            return true;    // Match
    }

   /*************************************************************
     * Function Name:  makeEmpty
     * Pre-conditions: BSTNode<T> *
     * Post-conditions: BSTNode<T> *
     * 
     * Internal method to make subtree empty.
     *******************************************/
    void makeEmpty( BSTNode<T> * & t )
    {
        if( t != nullptr )
        {
            makeEmpty( t->left );
            makeEmpty( t->right );
            delete t;
        }
        t = nullptr;
    }

   /*************************************************************
     * Function Name:  printTree
     * Pre-conditions:  BSTNode<T> *t, std::ostream & out
     * Post-conditions: void
     * 
     * Internal method to print a subtree rooted at t in sorted order.
     *****************************************************************/
    void printTree( BSTNode<T> *t, std::ostream & out ) const
    {
        if( t != nullptr )
        {
            printTree( t->left, out );
            out << t->element << " ";
            printTree( t->right, out );
        }
    }

   /******************************************
     * Function Name:  clone
     * Pre-conditions:  BSTNode<T> *t
     * Post-conditions: void
     * 
     * Internal method to clone subtree.
     ****************************************/
    BSTNode<T> * clone( BSTNode<T> *t ) const
    {
        if( t == nullptr )
            return nullptr;
        else
            return new BSTNode<T>{ t->element, clone( t->left ), clone( t->right ) };
    }
};

#endif